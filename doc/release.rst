.. highlight:: shell-session

Deployment
==========

Ligo-raven is deployed in gwcelery once a new release is made and depedencies
are updated. Therefore, the focus of deployment is making a release and
getting it approved by the RAVEN review team and the `SCCB`_.

Making a new release
--------------------

Once all changes we want to include are made, we can prepare a release by
updating the changelog with correct date and version number on the master
branch. Make sure all changes are included and clearly described. Then, make
a commit with these changes labeled **Update CHANGES.rst**:: 

    $ git commit -a

and tag this with the version number::

    $ git tag vX.XX -m "Version X.XX"

where X.XX is the version number. Push these changes via::

    $ git push upstream && git push upstream --tags

Next, we can turn this release to a local tarball file::

    $ python setup.py sdist

We can check this is installable by::

    $ pip install dist/ligo-raven-X.XX.tar.gz

Alternatively we could create a .whl file, although this generally is not
preferred::

    $ python setup.py bdist_wheel

We can upload our release file to `PyPI`_ using twine, assuming having
proper permissions of this page::

    $ twine upload dist/ligo-raven-X.XX.tar.gz

Check this was uploaded correctly by checking the `PyPI`_ page. Lastly,
we can start preparing the next release by creating a new entry in
CHANGES.rst. This is done by using **unreleased** for the date and setting
the next version number in *setup.py*, with the commit name
**Back to development**.

Initiating review
-----------------

If a release is intended to run online during an operting run via `gwcelery`_,
this release must be reviewed first.

The RAVEN review team should be
contacted and some sort of review process arranged, whether via email or
calls. The members of this review team generally review pipelines under the
umbrella of the GRB group, so being involved with the group and knowning the
members is important.
This process may take some time and may require another release if
changes are requested, so plan accordingly.

After this has been approved by the RAVEN review team, an `SCCB`_ request
should be made, filling out the appropriate info and responding to the
reviewers on the issue if prompted. Once this request has been approved, the
release is all set to run in `gwcelery`_ on live data.

.. _`gwcelery`: https://igwn.readthedocs.io/projects/gwcelery/en/latest/
.. _`PyPI`: https://pypi.org/project/ligo-raven/
.. _`SCCB`: https://git.ligo.org/computing/sccb/-/issues/new
